# WikiLocation

WikiLocation is test app that can store locations and open them in the (modified) Wikipedia app

## Goal

The goal was to write a clean and basic app, and make it look okay within a short amount of time. 

## Functionality

Within the app you can search for addresses (with a delayed auto search), add them to the list and delete (incl swipe to delete). The project is also localised in English and Dutch

## Cocoapods

3rd party frameworks and libaries used:
```bash
- Google places API to implement autocomplete for searching locations
- Realm to make the locations persistent
- ARSLineProgress as a basic loader
```

![picture](screenshot.png) ![picture](screenshot_dark.png)