//
//  WikiTableHeaderView.swift
//  WikiLocation
//
//  Created by Danny Grob on 13/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit

class WikiTableHeaderView: UITableViewHeaderFooterView {

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        contentView.backgroundColor = UIColor.init(hex: Theme.Common.greyColor)
        
        textLabel?.font = UIFont.systemFont(ofSize: Dimensions.Text.bodyText)
        textLabel?.textColor = UIColor.init(hex: Theme.Common.darkGreyColor)
    }

}
