//
//  LocationTableViewCell.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var coordinateLabel: UILabel!
    
    var locationObject:WikiLocation? {
        didSet {
            self.locationTitleLabel?.text = locationObject?.title
            if let lat = locationObject?.latitude,let lon = locationObject?.longitude {
                self.coordinateLabel?.text = String(format: "lat: %.4f, long: %.4f", lat, lon)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
