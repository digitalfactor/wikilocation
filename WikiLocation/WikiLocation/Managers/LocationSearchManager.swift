//
//  LocationSearchManager.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMapsBase
import RealmSwift

class LocationSearchManager: NSObject {
    
    static let shared:LocationSearchManager = LocationSearchManager()

    func storedLocations() -> Results<WikiLocation> {
        return RealmManager.defaultRealm().objects(WikiLocation.self)
    }
    
    /// This function searches for locations matching `text`.
    ///
    /// Usage:
    ///
    ///     search("search this", aroundCoordinate:myCoordinate, completion: myCompletionBlock);
    ///
    /// - Parameter text: The subject to be search for.
    /// - Parameter aroundCoordinate: This will make sure only relevant search results are returned around this location first
    /// - Parameter completion: The completion block with the GMSAutocompletePrediction array. Will be nil if there is an error
    ///
    func search(text:String, aroundCoordinate:CLLocationCoordinate2D? = nil, completion:(([GMSAutocompletePrediction]?)->())? = nil) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        
        var bounds:GMSCoordinateBounds?
        
        if let coordinate = aroundCoordinate {
            let lat = coordinate.latitude
            let long = coordinate.longitude
            let offset = 200.0 / 1000.0;
            let latMax = lat + offset;
            let latMin = lat - offset;
            let lngOffset = offset * cos(lat * Double.pi / 200.0);
            let lngMax = long + lngOffset;
            let lngMin = long - lngOffset;
            let initialLocation = CLLocationCoordinate2D(latitude: latMax, longitude: lngMax)
            let otherLocation = CLLocationCoordinate2D(latitude: latMin, longitude: lngMin)
            
            bounds = GMSCoordinateBounds(coordinate: initialLocation, coordinate: otherLocation)
        }
        
        GMSPlacesClient.shared().autocompleteQuery(text, bounds: bounds, filter: filter, callback: {(results, error) -> Void in
             
            if let error = error {
                print("Autocomplete error \(error)")
                completion?(nil)
                return
            }
            
            completion?(results)
            
        })
    }
    
    /// This function finds the GMSPlace for a given `placeID`.
    ///
    /// Usage:
    ///
    ///     lookupPlaceId("placeId", completion:myCompletionOrNil);
    ///
    /// - Parameter placeId: The placeId to lookup
    /// - Parameter completion: The completion block with the GMSPlace. Will be nil if there is an error
    ///
    func lookupPlaceId(_ placeId:String, completion:((GMSPlace?)->())? = nil) {
        GMSPlacesClient.shared().lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                completion?(nil)
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                completion?(nil)
                return
            }
            
            completion?(place)
        })
    }
}
