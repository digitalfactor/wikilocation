//
//  RealmManager.swift
//  WikiLocation
//
//  Created by Danny Grob on 07/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import RealmSwift

class RealmManager {
    
    static func defaultRealm() -> Realm {
        let config = Realm.Configuration(schemaVersion: 1, migrationBlock: { migration, oldSchemaVersion in
            // potentially lengthy data migration
        })

        let realm = try! Realm(configuration: config)
        return realm
    }
    
    public static func write(_ writeClosure: () -> ()) {
        if defaultRealm().isInWriteTransaction {
            writeClosure()
            return
        }
        do {
            try defaultRealm().write {
                writeClosure()
            }
        } catch {
            print("Could not write to database: \(error.localizedDescription)")
        }
    }
}
