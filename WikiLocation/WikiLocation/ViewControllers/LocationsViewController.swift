//
//  LocationsViewController.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit

class LocationsViewController: UIViewController, UITableViewDelegate, LocationDatasourceDelegate, SearchViewDelegate, UISearchBarDelegate {

    weak var searchViewController: SearchViewController?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    private var datasource:LocationDataSource?
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setup() {
        self.title = "locations_title".translated()
        datasource = LocationDataSource()
        datasource?.delegate = self
        tableView.dataSource = datasource

        datasource?.showLocations()
        
        self.searchBar?.delegate = self
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchLocation))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(toggleEdit))
        
        self.containerView?.isUserInteractionEnabled = false
        
        self.tableView.register(WikiTableHeaderView.self, forHeaderFooterViewReuseIdentifier: "WikiTableHeaderView")
    }
    
    @objc func toggleEdit() {
        self.tableView.setEditing(!self.tableView.isEditing, animated: true)
        
        if (self.tableView.isEditing) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toggleEdit))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(toggleEdit))
        }
    }
    
    @objc func searchLocation() {
        showSearch()
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Open wiki app, otherwise show error
        
        if let location = self.datasource?.locationObjects[indexPath.row],
            let latitude = "\(location.latitude)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let longitude = "\(location.longitude)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
           
            var components = URLComponents()
            components.scheme = "wikipedia"
            components.host = "places"
            components.queryItems = [
                URLQueryItem(name: "latitude", value: latitude),
                URLQueryItem(name: "longitude", value: longitude)
            ]

            if let url = components.url {
                UIApplication.shared.open(url, options: [:]) { (success) in
                    if !success {
                        let alert = UIAlertController.init(title: "open_wiki_error_title".translated(), message: "open_wiki_error_message".translated(), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "button_ok".translated(), style: .default, handler: { (action) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "title_stored_locations".translated()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Dimensions.TableHeaderView.headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "WikiTableHeaderView")
        view?.textLabel?.text = self.tableView(tableView, titleForHeaderInSection: section)
        return view
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "button_delete".translated()) { (action, indexPath) in
            // delete item at indexPath
            self.datasource?.remove(at: indexPath)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }

        return [delete]
    }
    
    //MARK: - LocationDatasourceDelegate
    func locationDataDidUpdate(datasource: LocationDataSource) {
        self.tableView?.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SearchViewController {
            searchViewController = destination
            destination.delegate = self
            destination.view.isHidden = true
        }
    }
    
    //MARK: - SearchViewDelegate
    func searchCancelled(on: SearchViewController) {
        self.hideSearch()
    }
    
    func hideSearch() {
        //restore the button
        if (self.tableView.isEditing) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toggleEdit))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(toggleEdit))
        }
        
        UIView.animate(withDuration: 0.4, animations: {
            self.searchViewController?.view.alpha = 0
        }) { (success) in
            self.searchViewController?.view.isHidden = true
            self.containerView?.isUserInteractionEnabled = false
        }
    }
    
    func showSearch() {
        //hide the bar button
        self.navigationItem.rightBarButtonItem = nil
        
        searchViewController?.view.alpha = 0
        searchViewController?.view.isHidden = false
        searchViewController?.activate()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchViewController?.view.alpha = 1
        }) { (success) in
            self.containerView?.isUserInteractionEnabled = true
        }
    }
    
    func searchLocationAdded(location: WikiLocation, on: SearchViewController) {
        self.hideSearch()
        
        let indexPath = IndexPath(row: tableView.numberOfRows(inSection: 0), section: 0)
        
        tableView.beginUpdates()
        self.datasource?.insertLocation(location, update: false)
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
    }


}
