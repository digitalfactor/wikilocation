//
//  SearchViewController.swift
//  WikiLocation
//
//  Created by Danny Grob on 07/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import ARSLineProgress

protocol SearchViewDelegate:class {
    func searchCancelled(on: SearchViewController)
    func searchLocationAdded(location:WikiLocation, on: SearchViewController)
}

class SearchViewController: UIViewController, UITableViewDelegate, UISearchBarDelegate, SearchDatasourceDelegate {

    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptySearchView: UIView!
    @IBOutlet weak var emptySearchLabel: UILabel!
    
    private var dataSource:SearchDataSource?
    
    weak var delegate:SearchViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }

    
    func setup() {
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        
        dataSource = SearchDataSource()
        dataSource?.delegate = self
        self.tableView.dataSource = dataSource
        self.tableView.delegate = self
        
        self.tableViewHeightConstraint.constant = 0
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.searchBar.placeholder = "title_search_city_or_address".translated()
        self.emptySearchLabel.text = "no_results_found".translated()
    }
    
    /// This function activates the searchbar.
    public func activate() {
        searchBar.searchTextField.becomeFirstResponder()
    }
    
    private func scaleTableViewHeight() {
        self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //It's needed to refresh the tableHeight, because the view can be redrawn
        tableView.reloadData()
        tableView.layoutIfNeeded()
        self.tableViewHeightConstraint.constant = self.tableView.contentSize.height
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ARSLineProgress.show()
        if let placeID = self.dataSource?.searchObjects[indexPath.row].placeID {
            self.dataSource?.addLocationFrom(placeID: placeID)
            ARSLineProgress.showSuccess()
        } else {
            ARSLineProgress.showFail()
        }
    }
    
    //MARK: - SearchDatasourceDelegate
    func searchDataDidUpdate(datasource: SearchDataSource) {
        self.tableView.reloadData()
        self.scaleTableViewHeight()
        self.emptySearchView?.isHidden = true
    }
    
    func searchDataNoResults(datasource: SearchDataSource) {
        self.tableView.reloadData()
        self.scaleTableViewHeight()
        self.emptySearchView?.isHidden = false
    }
    
    func searchDataAdd(location: WikiLocation) {
        self.delegate?.searchLocationAdded(location: location, on: self)
    }
    
    //MARK: - UISearchbarDelegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            ARSLineProgress.show()
            self.dataSource?.searchAddress(searchText: searchText) { success in
                ARSLineProgress.hide()
            }
        }
    }
    
    var timer:Timer?
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if timer != nil, timer!.isValid {
            //we are typing too fast, delay the call to prevent searching every character
            timer?.invalidate()
        }
        //delay the call by 0.5 seconds
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
            if let searchText = searchBar.text {
                self.dataSource?.searchAddress(searchText: searchText)
            }
            
             self.timer = nil
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.delegate?.searchCancelled(on: self)
        self.searchBar?.text = ""
        self.tableViewHeightConstraint.constant = 0
        self.dataSource?.clearSearch()
    }

}
