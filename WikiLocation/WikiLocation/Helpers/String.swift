//
//  String.swift
//  WikiLocation
//
//  Created by Danny Grob on 07/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit

extension String {
    func translated() -> String {
        return NSLocalizedString(self, comment: self);
    }

}
