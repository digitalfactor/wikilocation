//
//  Dimensions.swift
//  WikiLocation
//
//  Created by Danny Grob on 14/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit

struct Theme {
    struct Common {
        static let tintColor:Int = 0x2687da
        static let greyColor:Int = 0xF2F2F2
        static let darkGreyColor:Int = 0x858585
    }
}

struct Dimensions {
    
    /**
     Common dimensions - used across the app
    */
    struct Common {
        
        /** Horizontal Margins from left and right edges of screen */
        static let horizontalMargin: CGFloat = 10.0
        
        /** Vertical Margin from top and bottom edges of screen */
        static let verticalMargin: CGFloat = 10.0
        
    }
    
    
    /**
     Text sizes - used across the app
     */
    struct Text {
        
        /** Title font size */
        static let title: CGFloat = 18.0

        /** Sub-title font size */
        static let subTitle: CGFloat = 15.0
        
        /** Body text font size */
        static let bodyText: CGFloat = 13.0
        
    }
    
    
    /**
      Rooms & Profesional Filters views
     */
    struct TableHeaderView {
        
        static let headerHeight: CGFloat = 30.0;
        
    }
    
    
    
}
