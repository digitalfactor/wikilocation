//
//  Collection.swift
//  WikiLocation
//
//  Created by Danny Grob on 07/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import Foundation

extension Collection {
    // Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

