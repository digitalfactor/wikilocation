//
//  AppDelegate.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSPlacesClient.provideAPIKey("AIzaSyDNgS72Iuy9G85-dKEQ0cluFzGlXu3iEe8")
        changeAppearance()
        return true
    }

    func changeAppearance() {
        if #available(iOS 13, *) {
            let coloredAppearance = UINavigationBarAppearance()
            coloredAppearance.configureWithOpaqueBackground()
            coloredAppearance.backgroundColor = UIColor(named: "TintColor")
            coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]

            UINavigationBar.appearance().standardAppearance = coloredAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
            
            UINavigationBar.appearance().tintColor = .white
        } else {

            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().barTintColor = UIColor.init(hex: 0x2687da)
            
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        }
        
    }
}

