//
//  LocationsDataSource.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import GooglePlaces

protocol LocationDatasourceDelegate:class {
    func locationDataDidUpdate(datasource:LocationDataSource);
}
class LocationDataSource: NSObject, UITableViewDataSource {
    
    var locationObjects:[WikiLocation] = []
    
    weak var delegate:LocationDatasourceDelegate?
    
    override init() {
        super.init()
        
        let storedLocations = LocationSearchManager.shared.storedLocations()
        if storedLocations.count == 0 {
            //fill the database with dummy data if we have no items (as demo)
            self.fillDummyData()
        } else {
            locationObjects = Array(storedLocations)
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as? LocationTableViewCell {
            cell.locationObject = locationObjects[indexPath.row]
            return cell
        }

        return UITableViewCell()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func fillDummyData() {
        //Create 2 dummy locations, store them and fill the array
        let utrechtLocation = WikiLocation(title: "Utrecht", latitude: 52.0933791, longitude: 5.1210082)
        let amsterdamLocation = WikiLocation(title: "Amsterdam", latitude: 52.371012, longitude: 4.8972677)
        
        RealmManager.write {
            RealmManager.defaultRealm().add([utrechtLocation, amsterdamLocation])
        }
        
        locationObjects = [utrechtLocation, amsterdamLocation];
    }
    
    public func showLocations() {
        self.delegate?.locationDataDidUpdate(datasource: self)
    }
    
    public func insertLocation(_ location:WikiLocation, update:Bool = true) {
        locationObjects.append(location);
        
        //store in database
        RealmManager.write {
            RealmManager.defaultRealm().add(location)
        }
        
        if update == true {
            self.delegate?.locationDataDidUpdate(datasource: self)
        }
    }
    
    public func remove(at indexPath:IndexPath) {
        let toRemove = locationObjects[indexPath.row]
        
        //remove from the database
        RealmManager.write {
        RealmManager.defaultRealm().delete(toRemove)
        }
        
        locationObjects.remove(at: indexPath.row)
    }
    
    
}
