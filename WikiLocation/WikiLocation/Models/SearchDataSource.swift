//
//  SearchDataSource.swift
//  WikiLocation
//
//  Created by Danny Grob on 07/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//

import UIKit
import GooglePlaces

protocol SearchDatasourceDelegate:class {
    func searchDataDidUpdate(datasource:SearchDataSource);
    func searchDataNoResults(datasource:SearchDataSource);
    func searchDataAdd(location: WikiLocation)
}

class SearchDataSource: NSObject, UITableViewDataSource {
    var searchObjects:[GMSAutocompletePrediction] = []
    weak var delegate:SearchDatasourceDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as? SearchTableViewCell {
            let searchObject = searchObjects[indexPath.row]
            cell.resultTitleLabel?.attributedText = searchObject.attributedPrimaryText
            cell.resultSubLabel?.attributedText = searchObject.attributedSecondaryText
            return cell
        }
    
        return UITableViewCell()
    }
    
    public func addLocationFrom(placeID:String, named:String? = nil) {
        LocationSearchManager.shared.lookupPlaceId(placeID) { (place) in
            guard place != nil else {
                //log error
                return
            }
            
            //We make it explicitly unwrapped because we make sure to set it in the else
            var title:String! = named;
            
            if title == nil, let placeName = place?.name {
                title = placeName
            } else if title == nil {
                //We got no title
                title = "No title"
            }
        
            let location = WikiLocation(title: title, latitude: place!.coordinate.latitude, longitude: place!.coordinate.longitude)
            self.delegate?.searchDataAdd(location: location)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func searchAddress(searchText:String, completion:((Bool)->())? = nil) {
        LocationSearchManager.shared.search(text: searchText) { results in
            if let searchResults = results, searchResults.count > 0 {
                self.searchObjects = searchResults
                self.delegate?.searchDataDidUpdate(datasource: self)
                completion?(true)
            } else if results?.count == 0 {
                self.searchObjects = []
                self.delegate?.searchDataNoResults(datasource: self)
                completion?(false)
            }
            
        }
    }
    
    public func clearSearch() {
        self.searchObjects = []
    }

}
