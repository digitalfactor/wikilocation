//
//  WikiLocation.swift
//  WikiLocation
//
//  Created by Danny Grob on 05/11/2019.
//  Copyright © 2019 Danny Grob. All rights reserved.
//
//  Simple Realm Object to store a title, latitude and longitude

import UIKit
import MapKit
import RealmSwift

class WikiLocation: Object {
    @objc dynamic var title:String?
    @objc dynamic var latitude:Double = 0
    @objc dynamic var longitude:Double = 0
    
    convenience init(title:String, latitude:Double, longitude:Double) {
        self.init();
        
        self.title = title
        self.latitude = latitude
        self.longitude = longitude
    }
}
